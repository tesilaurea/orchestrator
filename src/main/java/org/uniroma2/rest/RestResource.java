package org.uniroma2.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.uniroma2.model.VertexConcrete;
import org.uniroma2.repositories.ResourceRepository;

@RestController
@RequestMapping("/resource/redis/")
public class RestResource {

	@Autowired
	private ResourceRepository resourceRepository;

	/*@RequestMapping("/save")
	public String save() {
		// save a single Customer
		resourceRepository.save(new VertexConcrete("1", "S1", false,"Service1", "Node1", "127.0.0.1", 1.0, 1.0));
		resourceRepository.save(new VertexConcrete("2", "S2", false,"Service2", "Node2", "127.0.0.2", 1.0, 1.0));
		resourceRepository.save(new VertexConcrete("3", "S3", false,"Service3", "Node2", "127.0.0.3", 1.0, 1.0));
		resourceRepository.save(new VertexConcrete("4", "S4", false,"Service4", "Node3", "127.0.0.4", 1.0, 1.0));
		resourceRepository.save(new VertexConcrete("5", "S5", false,"Service5", "Node4", "127.0.0.5", 1.0, 1.0));
		return "Done";
	}*/

	@RequestMapping("/findall")
	public String findAll() {
		String result = "";
		Map<String, VertexConcrete> nodes = resourceRepository.findAll();

		for (VertexConcrete node : nodes.values()) {
			result += node.toString() + "<br>";
		}

		return result;
	}

	@RequestMapping("/find")
	public String findById(@RequestParam("uid")String uid) {
		String result = "";
		result = resourceRepository.find(uid).toString();
		return result;
	}

	/*@RequestMapping(value = "/uppercase")
	public String postCustomer(@RequestParam("id") Long id) {
		Customer customer = customerRepository.find(id);
		customer.setFirstName(customer.getFirstName().toUpperCase());
		customer.setLastName(customer.getLastName().toUpperCase());

		customerRepository.update(customer);

		return "Done";
	}*/

	@RequestMapping("/delete")
	public String deleteById(@RequestParam("uid") String uid) {
		resourceRepository.delete(uid);

		return "Done";
	}
}


