package org.uniroma2.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.uniroma2.model.EdgeConcrete;
import org.uniroma2.model.VertexConcrete;
import org.uniroma2.repositories.LinkRepository;

@RestController
@RequestMapping("/link/redis/")
public class RestLink {
	
	@Autowired
	private LinkRepository linkRepository;
	
	/*@RequestMapping("/save")
	public String save() {
		
		linkRepository.save(new EdgeConcrete("(2-3)",
				new VertexConcrete("2", "S2", false,"Service2", "Node1", 1.0, 1.0),
				new VertexConcrete("3", "S3", false,"Service3", "Node2", 1.0, 1.0), 
				12.0,
				1.0)
		);
		//linkRepository.save(new Resource(2L, "S2", false,"Service2"));
		//linkRepository.save(new Resource(3L, "S3", false,"Service3"));
		//linkRepository.save(new Resource(4L, "S4", false,"Service4"));
		//linkRepository.save(new Resource(5L, "S5", false,"Service5"));
		return "Done";
	}*/

	@RequestMapping("/findall")
	public String findAll() {
		String result = "";
		Map<String, EdgeConcrete> links = linkRepository.findAll();

		for (EdgeConcrete link : links.values()) {
			result += link.toString() + "<br>";
		}

		return result;
	}

	@RequestMapping("/find")
	public String findById(@RequestParam("uid") String uid) {
		String result = "";
		result = linkRepository.find(uid).toString();
		return result;
	}

	/*@RequestMapping(value = "/uppercase")
	public String postCustomer(@RequestParam("id") Long id) {
		Customer customer = customerRepository.find(id);
		customer.setFirstName(customer.getFirstName().toUpperCase());
		customer.setLastName(customer.getLastName().toUpperCase());

		customerRepository.update(customer);

		return "Done";
	}*/

	@RequestMapping("/delete")
	public String deleteById(@RequestParam("uid") String uid) {
		linkRepository.delete(uid);

		return "Done";
	}

}
