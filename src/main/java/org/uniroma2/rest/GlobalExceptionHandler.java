package org.uniroma2.rest;

import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.uniroma2.model.rest.DTO;

import java.io.IOException;

import org.springframework.beans.TypeMismatchException;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;



/**
 * Questa classe è pensata per intercettare le eccezioni sollevate a runtime per poterle
 * gestire restituendo un Json contenente un messaggio di errore ed un errorCode.
 * 
 * Tra le eccezioni gestite, ci sono anche quelle customizzate all'interno dell'applicazione.
 * Si decide per le eccezioni a runtime restituire HttpStatus.INTERNAL_SERVER_ERROR
 * perché sono pensate come un errore dell'applicazione stessa.
 * 
 * Si riportano brevemente i significati degli stereotipi con cui è annotata la classe.
 * @ControllerAdvice tells your spring application that this class will do the exception 
 * handling for your application.
 * @RestController will make it a controller and let this class render the response.
 * Use @ExceptionHandler annotation to define the class of Exception it will catch. 
 * (A Base class will catch all the Inherited and extended classes)
 * You can set the response status for exception using @ResponseStatus annotation.
 * 
 * @author Fabio Alberto Coira
 *
 */

@ControllerAdvice
@RestController
public class GlobalExceptionHandler {
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<DTO> exceptionHandler(Exception ex) {
		ex.printStackTrace();// DEBUG
		DTO error = new DTO();
		error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		error.setMessage(ex.getClass().toString()+": " + ex.getMessage());
		return new ResponseEntity<DTO>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	//this will handle situation when there's exception during binding, 
    // for example you except number and user passess string (A123.00 for example)
    @ExceptionHandler(TypeMismatchException.class)
    public ResponseEntity<DTO> typeMismatchException(Exception ex){  
    	ex.printStackTrace();// DEBUG
		DTO error = new DTO();
		error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		error.setMessage(ex.getClass().toString()+": " + ex.getMessage());
		
		return new ResponseEntity<DTO>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
	@ExceptionHandler(HttpClientErrorException.class)
	public ResponseEntity<DTO> httpClientErrorExceptionHandler(Exception ex) {
		ex.printStackTrace();// DEBUG
		DTO error = new DTO();
		error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		error.setMessage(ex.getClass().toString()+": " + ex.getMessage());
		return new ResponseEntity<DTO>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	 
    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<DTO> illegalArgumentException(Exception ex){ 
    	ex.printStackTrace();// DEBUG
		DTO error = new DTO();
		error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		error.setMessage(ex.getMessage());
		return new ResponseEntity<DTO>(error, HttpStatus.INTERNAL_SERVER_ERROR);  
    }
    
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<DTO> missingServletRequestParameterException(Exception ex) {
    	ex.printStackTrace();// DEBUG
		DTO error = new DTO();
		error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		error.setMessage(ex.getMessage());
		return new ResponseEntity<DTO>(error, HttpStatus.INTERNAL_SERVER_ERROR);  
    }  
    
   
    @ExceptionHandler(JsonParseException.class)
    public ResponseEntity<DTO> jsonParseException(Exception ex) {
    	ex.printStackTrace();// DEBUG
		DTO error = new DTO();
		error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		error.setMessage(ex.getMessage());
		return new ResponseEntity<DTO>(error, HttpStatus.INTERNAL_SERVER_ERROR);  
    }
    
    @ExceptionHandler(JsonMappingException.class)
    public ResponseEntity<DTO> jsonMappingException(Exception ex) {
    	ex.printStackTrace();// DEBUG
		DTO error = new DTO();
		error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		error.setMessage(ex.getMessage());
		return new ResponseEntity<DTO>(error, HttpStatus.INTERNAL_SERVER_ERROR);   
    } 
    
    @ExceptionHandler(IOException.class)
    public ResponseEntity<DTO> iOException(Exception ex) {
    	ex.printStackTrace();// DEBUG
		DTO error = new DTO();
		error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		error.setMessage(ex.getMessage());
		return new ResponseEntity<DTO>(error, HttpStatus.INTERNAL_SERVER_ERROR);   
    } 
    
    @ExceptionHandler(DataAccessException.class)
    public ResponseEntity<DTO> dataAccessException(Exception ex) {
    	ex.printStackTrace();// DEBUG
		DTO error = new DTO();
		error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		error.setMessage(ex.getMessage());
		return new ResponseEntity<DTO>(error, HttpStatus.INTERNAL_SERVER_ERROR);   
    }
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public  ResponseEntity<DTO> httpMessageNotReadableException(Exception ex) {
    	ex.printStackTrace();// DEBUG
		DTO error = new DTO();
		error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		error.setMessage(ex.getMessage());
		return new ResponseEntity<DTO>(error, HttpStatus.INTERNAL_SERVER_ERROR);   
    }
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public  ResponseEntity<DTO> httpRequestMethodNotSupportedException(Exception ex) {
    	ex.printStackTrace();// DEBUG
		DTO error = new DTO();
		error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		error.setMessage(ex.getMessage());
		return new ResponseEntity<DTO>(error, HttpStatus.INTERNAL_SERVER_ERROR);  
    }
  
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<DTO> methodArgumentNotValidExceptionn(Exception e) {
    	DTO error = new DTO();
    	error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
    	error.setMessage(e.getMessage());
    	ResponseEntity<DTO> responseEntity = 
				new ResponseEntity<DTO>(
						error, HttpStatus.valueOf(error.getErrorCode()));
		return responseEntity;
    	 
    }
}
