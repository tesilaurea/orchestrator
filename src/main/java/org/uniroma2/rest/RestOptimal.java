package org.uniroma2.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.uniroma2.controller.OptimalService;
import org.uniroma2.model.rest.DTOWorkflow;
import org.uniroma2.model.rest.response.DTOResponseConcreteMap;
import org.uniroma2.model.rest.response.DTOResponseTuningParams;
import org.uniroma2.utils.GlobalConst;

@RestController
@RequestMapping("/optimal/")
public class RestOptimal {
	
	/** The strategy service. */
	@Autowired
	OptimalService optimalService;
	
	@RequestMapping(value = "/submit/workflow", 
			method = RequestMethod.POST)
	public ResponseEntity<?> submitWorkflow(@RequestBody DTOWorkflow dtoWorkflow) {
		
		return optimalService.submitWorkflow(dtoWorkflow);
	}
	
	@RequestMapping(value = "/submit/workflow/v2", 
			method = RequestMethod.POST)
	public ResponseEntity<?> submitWorkflowV2(@RequestBody DTOWorkflow dtoWorkflow) {
		
		return optimalService.submitWorkflowV2(dtoWorkflow);
	}
	@RequestMapping(value = "/set/weight", method = RequestMethod.PUT)
	public ResponseEntity<?> setWeight(@RequestParam("wa") String wa, 
			@RequestParam("wr") String wr) {
			GlobalConst.WEIGHT_AVAILABILITY = wa;
			GlobalConst.WEIGHT_RESP_TIME = wr;
			DTOResponseTuningParams dtoResponseTuningParams = 
					new DTOResponseTuningParams();
			dtoResponseTuningParams.setWa(wa);
			dtoResponseTuningParams.setWr(wr);
		ResponseEntity<DTOResponseTuningParams> responseEntity = 
				new ResponseEntity<DTOResponseTuningParams>
					(dtoResponseTuningParams,HttpStatus.OK);
		return responseEntity;
	}
	@RequestMapping(value = "/get/weight", method = RequestMethod.GET)
	public ResponseEntity<?> getWeight() {
			DTOResponseTuningParams dtoResponseTuningParams = 
					new DTOResponseTuningParams();
			dtoResponseTuningParams.setWa(GlobalConst.WEIGHT_AVAILABILITY);
			dtoResponseTuningParams.setWr(GlobalConst.WEIGHT_RESP_TIME);
		ResponseEntity<DTOResponseTuningParams> responseEntity = 
				new ResponseEntity<DTOResponseTuningParams>
					(dtoResponseTuningParams,HttpStatus.OK);
		return responseEntity;
	}
}

