package org.uniroma2.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.uniroma2.model.RedisMicroservice;
import org.uniroma2.repositories.MicroserviceRepository;


@RestController
@RequestMapping("/microservice/redis/")
public class RestMicroservice {
	@Autowired
	private MicroserviceRepository microserviceRepository;
	
	@RequestMapping("/findall")
	public String findAll() {
		String result = "";
		Map<String, RedisMicroservice> microservices = microserviceRepository.findAll();

		for (RedisMicroservice m : microservices.values()) {
			result += m.toString() + "<br>";
		}

		return result;
	}

	@RequestMapping("/find")
	public String findById(@RequestParam("id") String id) {
		String result = "";
		result = microserviceRepository.find(id).toString();
		return result;
	}
	
	@RequestMapping("/delete")
	public String deleteById(@RequestParam("id") String id) {
		microserviceRepository.delete(id);

		return "Done";
	}
}
