package org.uniroma2.controller;

import org.springframework.http.ResponseEntity;
import org.uniroma2.model.rest.DTOWorkflow;
import org.uniroma2.model.rest.response.DTOResponseConcreteMap;

public interface OptimalService {

	ResponseEntity<?> submitWorkflow(DTOWorkflow dtoWorkflow);

	ResponseEntity<?> submitWorkflowV2(DTOWorkflow dtoWorkflow);

}
