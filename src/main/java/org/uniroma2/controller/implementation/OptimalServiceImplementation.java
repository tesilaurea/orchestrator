package org.uniroma2.controller.implementation;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.uniroma2.controller.OptimalService;
import org.uniroma2.model.EdgeAbstract;
import org.uniroma2.model.EdgeConcrete;
import org.uniroma2.model.JoinVertexResource;
import org.uniroma2.model.Microservice;
import org.uniroma2.model.RedisMicroservice;
import org.uniroma2.model.VertexAbstract;
import org.uniroma2.model.VertexConcrete;
import org.uniroma2.model.rest.DTOWorkflow;
import org.uniroma2.model.rest.response.DTOResponseConcreteMap;
import org.uniroma2.repositories.LinkRepository;
import org.uniroma2.repositories.MicroserviceRepository;
import org.uniroma2.repositories.ResourceRepository;
import org.uniroma2.tesi.App;
import org.uniroma2.tesi.codd.MSoaGraphBuilder;
import org.uniroma2.tesi.codd.ODDException;
import org.uniroma2.tesi.codd.ODDParameters;
import org.uniroma2.tesi.codd.ResGraphBuilder;
import org.uniroma2.tesi.codd.metricsprovider.ApplicationMetricsProvider;
import org.uniroma2.tesi.codd.metricsprovider.ResourceMetricsProvider;
import org.uniroma2.tesi.codd.metricsprovider.SimpleApplicationMetricsProvider;
import org.uniroma2.tesi.codd.metricsprovider.SimpleResourceMetricsProvider;
import org.uniroma2.tesi.codd.model.MSoaEdge;
import org.uniroma2.tesi.codd.model.MSoaPath;
import org.uniroma2.tesi.codd.model.MSoaVertex;
import org.uniroma2.tesi.codd.model.OptimalSolution;
import org.uniroma2.tesi.codd.model.Pair;
import org.uniroma2.tesi.codd.model.ServiceEdge;
import org.uniroma2.tesi.codd.model.ServiceVertex;
import org.uniroma2.tesi.codd.placement.ODDModel.MODE;
import org.uniroma2.utils.GlobalConst;
import org.uniroma2.utils.Utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Service("OptimalService")
public class OptimalServiceImplementation 
		implements OptimalService{
	@Autowired
	private ResourceRepository resourceRepository;
	@Autowired
	private LinkRepository linkRepository;
	@Autowired
	private MicroserviceRepository microserviceRepository;
	
	@Override
	public ResponseEntity<?> submitWorkflow(DTOWorkflow dtoWorkflow) {
		
		// Prendo le liste da Redis e dall' input dell'utente
		LinkedList<VertexAbstract> services = dtoWorkflow.getServices();
		LinkedList<EdgeAbstract> connections = dtoWorkflow.getConnections();
		Map<String, VertexConcrete> nodes = resourceRepository.findAll();
		Map<String, EdgeConcrete> edges = linkRepository.findAll();	
		LinkedList<VertexConcrete> nodesList = new LinkedList<VertexConcrete>
		(nodes.values());
		LinkedList<EdgeConcrete> edgesList = new LinkedList<EdgeConcrete>
		(edges.values());
		
		// rivedere questo params qui
		ODDParameters params = new ODDParameters(MODE.BASIC);
		App.setParameters(params);
		
		
		// Passo da VertexConcrete e EdgeConcrete a quelli della libreria CPLEX
		
		LinkedList<ServiceEdge> serviceEdgeList = new LinkedList<ServiceEdge>();
		LinkedList<ServiceVertex> serviceVertexList = new LinkedList<ServiceVertex>();
		int k = 0;
		for(VertexConcrete v:nodesList){			
			ServiceVertex s= new ServiceVertex(
					k,//Integer.parseInt(v.getUid()),
					v.getUid(),
					v.getType(), 
					v.getNode(), 
					v.getIp(),
					v.getSpeedup(), 
					v.getAvailability());
		
			serviceVertexList.add(s);
			k++;
			System.out.println(s.toString());
		}
		
		for(EdgeConcrete e:edgesList){
			ServiceVertex from = null;
			ServiceVertex to = null;
			for(ServiceVertex s:serviceVertexList){
				if(s.getId().equals(e.getFrom().getUid())){
					from = s;
					break;
				}
			}
			for(ServiceVertex s:serviceVertexList){
				if(s.getId().equals(e.getTo().getUid())){
					to = s;
					break;
				}
			}
			ServiceEdge s= new ServiceEdge(
					//Integer.parseInt(e.getFrom().getUid()), 
					//Integer.parseInt(e.getTo().getUid()), 
					from.getIndex(),
					to.getIndex(),
					new Double(e.getDelay()).intValue(), e.getAvailability());
			serviceEdgeList.add(s);
		}
		
		/*
		//service 0 node 0
		serviceVertexList.add(ResGraphBuilder.createService(0, 0, 0));
		//service 1 node 1
		serviceVertexList.add(ResGraphBuilder.createService(1, 1, 1));
		//service 2 node 2
		serviceVertexList.add(ResGraphBuilder.createService(2, 2, 2));
		//service 3 node 0
		serviceVertexList.add(ResGraphBuilder.createService(3, 0, 3));
		//service 4 node 1
		serviceVertexList.add(ResGraphBuilder.createService(4, 1, 4));
		//service 4 node 0
		serviceVertexList.add(ResGraphBuilder.createService(4, 0, 5));
		//service 3 node 2
		serviceVertexList.add(ResGraphBuilder.createService(3, 1, 6));
		//service 0 node 2
		serviceVertexList.add(ResGraphBuilder.createService(0, 2, 7));
		//service 4 node 2
		serviceVertexList.add(ResGraphBuilder.createService(4, 2, 8));
		//service 3 node 2
		serviceVertexList.add(ResGraphBuilder.createService(3, 2, 9));
		
		ResourceMetricsProvider rmp = 
				new SimpleResourceMetricsProvider(params);
		for (ServiceVertex u : serviceVertexList){
			for (ServiceVertex v : serviceVertexList){
				int uIndex = u.getIndex();
				int vIndex = v.getIndex();
	
				int latUV = (int) rmp.getSerLatency(u.getNode(), v.getNode());// getLat(u.getNode(), v.getNode()); //(int) metricsProvider.getLatency(uIndex, vIndex);
				double bigAUV = 1.0; // metricsProvider.getAvailability(uIndex, vIndex);
				
				ServiceEdge rEdge = new ServiceEdge(
						uIndex, 
						vIndex, latUV, bigAUV);
				serviceEdgeList.add(rEdge);
			}
		}*/
		
		ResGraphBuilder rbuilder = new ResGraphBuilder();
		Map<Integer, ServiceVertex> vRes = 
				ResGraphBuilder.createExternalServiceVertexMap(
				serviceVertexList);
		Map<Pair, ServiceEdge> eRes = ResGraphBuilder.createExternalServiceEdgeMap(
				serviceEdgeList);
	    ResGraphBuilder r= rbuilder.createExternalServicePod(vRes, eRes);
	    
	    rbuilder.printSerGraph(true);
	    
	    // Passo a preparare le risorse astratte del DAG per renderle idonee
	    // all'uso per la libreria CPLEX
	    
		MSoaGraphBuilder mbuilder = new MSoaGraphBuilder();
		LinkedList<MSoaEdge> msoaEdgeList = 
					new LinkedList<MSoaEdge>();
		LinkedList<MSoaVertex> msoaVertexList =  new LinkedList<MSoaVertex>();
		
		MSoaGraphBuilder.TYPE msoaType = MSoaGraphBuilder.TYPE.CUSTOM;
		
		ApplicationMetricsProvider amp = new SimpleApplicationMetricsProvider(
				msoaType, services.size(), params);
		int count = 0;
		for(VertexAbstract n: services){			
			msoaVertexList.add(	
					//l'id dell'abstract vertex è il tipo.. da verificare
					MSoaGraphBuilder.createVertex(n.getType(), amp, count, n.getName()));
			count++;
		}
		
		Map<Integer, MSoaVertex> vDsp =  
				MSoaGraphBuilder.createExternalMSoaVertexMap(
		    		msoaVertexList);
		
		List<Integer> sourcesIndex = new ArrayList<Integer>();
		List<Integer> sinksIndex = new ArrayList<Integer>();
		
		/* 2. Set node 0 as source */
		sourcesIndex.add(new Integer(0));
		
		//Per integrare anche gli edges, che per ora sono unicamente sequenziali!
		//per ora basta prendere gli archi che vengono dall'esterno e aggiungerli
		//nella lista.. per ora li farei inviare in ordine di come sono 
		//da un punto di vista logico sperando (ma non credo) che cambi qualcosa
		
		/* 3. Create DspEdges to represent link between executors */
		/*int[] prevLevel = new int[] { 0 };
		for (int j = 1; j < 5; j++) {
			for (int i : prevLevel) {
				MSoaEdge ij = new MSoaEdge(i, j);
				msoaEdgeList.add(ij);
			}
			prevLevel = new int[] { j };
		}*/
		
		/*
		 * 
		 * 
		 * */
		
		for(EdgeAbstract e : connections){
			VertexAbstract from =  e.getFrom();
			VertexAbstract to =  e.getTo();
			int f = -1;
			int t = -1;
			for (MSoaVertex m : msoaVertexList){
				if(m.getType().equals(from.getType()) && 
						m.getName().equals(from.getName())){
					/*System.out.println("from: ");
					System.out.println(m.getType());
					System.out.println(from.getType());
					System.out.println(m.getIndex());
					System.out.println("__________________");*/
					f = m.getIndex();
				}
			}
			for (MSoaVertex m : msoaVertexList){
				if(m.getType().equals(to.getType()) && 
						m.getName().equals(to.getName())){
					/*System.out.println("to: ");
					System.out.println(m.getType());
					System.out.println(to.getType());
					System.out.println(m.getIndex());
					System.out.println("__________________");*/
					t = m.getIndex();
				}
			}
			MSoaEdge ij = new MSoaEdge(f, t);
			msoaEdgeList.add(ij);
			System.out.println(f + " " + t);
	
		}
		
		//---------------------------------------------------------------------
		Map<Pair, MSoaEdge> eDsp =  MSoaGraphBuilder.createExternalMSoaEdgeMap(
	    		msoaEdgeList);
		/* 4. Compute Paths */
		ArrayList<MSoaPath> paths = mbuilder.computePaths(vDsp, eDsp, sourcesIndex);
	
		/* 5. Identify sinks */
		for (MSoaPath path : paths) {
			sinksIndex.add(path.getSink());
		}
		
		MSoaGraphBuilder m = mbuilder.createExternalTopology(
				vDsp,
				eDsp, 
				sourcesIndex,
				sinksIndex,
				paths);
	    m.setCandidate(r);
	    m.printGraph();
	    OptimalSolution op = null;
	    try {
			op = App.optimalMSOAOrchestrationResolver(
					Double.parseDouble(GlobalConst.WEIGHT_AVAILABILITY),
					Double.parseDouble(GlobalConst.WEIGHT_RESP_TIME),
					r,
					m,
					params
					);
			System.out.println(op.toString());
			System.out.println(op.toUidString(r));
			System.out.println(op.getSolution(r));
			System.out.println(op.getSolutionVertex(r));
			System.out.println(op.getSolutionEdge(r));
		} catch (ODDException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    // A QUESTO PUNTO VA INSERITA LA JOIN TRA LA SOLUZIONE 
	    // E LE INFO CONTENUTE NEL GRAFO ASTRATTO

		List<ServiceVertex> servicesList =	
				new LinkedList<ServiceVertex>(
						op.getSolutionVertex(r).values());
	
		LinkedList<ServiceEdge> connectionsList = 
				new LinkedList<ServiceEdge>(
						op.getSolutionEdge(r).values());
		DTOResponseConcreteMap dtoResponseConcreteMap  =
					new DTOResponseConcreteMap(); 
		dtoResponseConcreteMap.setServices(new LinkedList<VertexConcrete>());
		dtoResponseConcreteMap.setJoin(new LinkedList<JoinVertexResource>());
		
		for(ServiceVertex s: servicesList){
			VertexConcrete v = new VertexConcrete();
			v.setIp(s.getIp());
			v.setType(s.getType());
			v.setNode(s.getNode());
			v.setUid(s.getId());
			v.setSpeedup(s.getSpeedup());
			v.setAvailability(s.getAvailability());
			dtoResponseConcreteMap.addVertex(v);
		}
		/*for(VertexAbstract n: services){			
			msoaVertexList.add(	
					//l'id dell'abstract vertex è il tipo.. da verificare
					MSoaGraphBuilder.createVertex(n.getType(), amp, count, n.getName()));
			count++;
		}*/
		
		for(MSoaVertex l : msoaVertexList){
			System.out.println(l.toString());
			ServiceVertex sv = null; 
			for(ServiceVertex s: servicesList){
				if(s.getIndex() == op.getPlacement(l.getIndex())){
					sv = s;
					break;
				}
			}
			JoinVertexResource j = new JoinVertexResource();
			
			j.setIp(sv.getIp());
			j.setType(sv.getType());
			j.setNode(sv.getNode());
			j.setUid(sv.getId());
			j.setSpeedup(sv.getSpeedup());
			j.setAvailability(sv.getAvailability());
			VertexAbstract va = null;
			for(VertexAbstract n: services){	
				if(n.getName().equals(l.getName())){
					va = n;
					break;
				}
			}
			
			j.setName(va.getName());
			j.setMethod(va.getMethod());
			//j.setUrl(va.getUrl);
			j.setOrder(va.getOrder());
			j.setTo(va.getTo());
			j.setMicroservice(va.getMicroservice());
			j.setIsbody(va.isIsbody());
			j.setBody(va.getBody());
			j.setIsparameters(va.isIsparameters());
			j.setParameters(va.getParameters());
			j.setIsurlconcat(va.isIsurlconcat());
			j.setUrlconcat(va.getUrlconcat());
			j.setEnd(va.isEnd());
			j.setIsbound(va.isIsbound());
			
			
			j.setToip(va.getToip());
			j.setTomicroservices(va.getTomicroservices());
			j.setIstourlconcat(va.getIstourlconcat());
			j.setTourlconcat(va.getTourlconcat());
			j.setIstoparameters(va.getIstoparameters());
			j.setToparameters(va.getToparameters());
			
			//j.setBoundto(va.isIsbound());
			
			dtoResponseConcreteMap.addJoin(j); 
		}
			
			
		
		
		
		/*for(ServiceEdge s: connectionsList){
			EdgeConcrete e = new EdgeConcrete();
			e.setFrom(s.getFrom());
			e.setTo(s.getTo());
			
			dtoResponseConcreteMap.addVertex(v);
		}*/
			
		ResponseEntity<DTOResponseConcreteMap> responseEntity = 
						new ResponseEntity<DTOResponseConcreteMap>(dtoResponseConcreteMap,HttpStatus.OK);
		return responseEntity;
	}

	@Override
	public ResponseEntity<?> submitWorkflowV2(DTOWorkflow dtoWorkflow) {
		// TODO Auto-generated method stub
		// Prendo le liste da Redis e dall' input dell'utente
		LinkedList<VertexAbstract> services = dtoWorkflow.getServices();
		LinkedList<EdgeAbstract> connections = dtoWorkflow.getConnections();
		
		Map<String, VertexConcrete> nodes = resourceRepository.findAll();
		Map<String, EdgeConcrete> edges = linkRepository.findAll();	
		
		LinkedList<VertexConcrete> nodesList = new LinkedList<VertexConcrete>
		(nodes.values());
		LinkedList<EdgeConcrete> edgesList = new LinkedList<EdgeConcrete>
		(edges.values());
		
		for(VertexConcrete vc : nodesList){
			for(VertexAbstract va : services){
				if(va.getType().equals(vc.getType())){
					RedisMicroservice r = microserviceRepository.find(vc.getUid()+
							"|"+vc.getType()+
							"|"+va.getMicroservice());
					//appendi alla lista un nuovo elemento <micro, tu>
					vc.setMicroservices(new Microservice(r.getName(), r.getTu()));
				}
			}
		}
		
		// rivedere questo params qui (Li setto dopo!)
		ODDParameters params = new ODDParameters(MODE.BASIC);
		App.setParameters(params);
		
		// Passo da VertexConcrete e EdgeConcrete a quelli della libreria CPLEX
		
		LinkedList<ServiceEdge> serviceEdgeList = new LinkedList<ServiceEdge>();
		LinkedList<ServiceVertex> serviceVertexList = new LinkedList<ServiceVertex>();
		
		int k = 0;
		for(VertexConcrete v:nodesList){	
			
			ServiceVertex s= new ServiceVertex(
					k,//Integer.parseInt(v.getUid()),
					v.getUid(),
					v.getType(), 
					v.getNode(), 
					v.getIp(),
					v.getSpeedup(), 
					v.getAvailability(),
					0,
					v.getConvMicroservices());
					// qui aggiungo la lista dei microservizi con i tu
			serviceVertexList.add(s);
			k++;
			System.out.println(s.toString());
		}
		
		for(EdgeConcrete e:edgesList){
			ServiceVertex from = null;
			ServiceVertex to = null;
			for(ServiceVertex s:serviceVertexList){
				if(s.getId().equals(e.getFrom().getUid())){
					from = s;
					break;
				}
			}
			for(ServiceVertex s:serviceVertexList){
				if(s.getId().equals(e.getTo().getUid())){
					to = s;
					break;
				}
			}
			//la disponibilità dell'arco la passo da Redis
			ServiceEdge s= new ServiceEdge(
					//Integer.parseInt(e.getFrom().getUid()), 
					//Integer.parseInt(e.getTo().getUid()), 
					from.getIndex(),
					to.getIndex(),
					new Double(e.getDelay()).intValue(), 
					e.getAvailability());
			serviceEdgeList.add(s);
		}
		
		//Le aggiorno alla V2
		ResGraphBuilder rbuilder = new ResGraphBuilder();
		Map<Integer, ServiceVertex> vRes = 
				ResGraphBuilder.createExternalServiceVertexMap(
				serviceVertexList);
		Map<Pair, ServiceEdge> eRes = ResGraphBuilder.createExternalServiceEdgeMap(
				serviceEdgeList);
	    ResGraphBuilder r= rbuilder.createExternalServicePod(vRes, eRes);
	    
	    rbuilder.printSerGraph(true);
	    
	    //Aggiorno il caso astratto, ma devo occuparmi della V2
	    MSoaGraphBuilder mbuilder = new MSoaGraphBuilder();
		LinkedList<MSoaEdge> msoaEdgeList = 
					new LinkedList<MSoaEdge>();
		LinkedList<MSoaVertex> msoaVertexList =  new LinkedList<MSoaVertex>();
		
		MSoaGraphBuilder.TYPE msoaType = MSoaGraphBuilder.TYPE.CUSTOM;
		
		ApplicationMetricsProvider amp = new SimpleApplicationMetricsProvider(
				msoaType, services.size(), params);
		// prestare attenzione se l'ordine in services dei VertexAbstract non è corretto
		int count = 0;
		for(VertexAbstract n: services){			
			msoaVertexList.add(	
					MSoaGraphBuilder.createVertex(n.getType(), amp, count, n.getName(), n.getMicroservice()));
			count++;
		}
		
		Map<Integer, MSoaVertex> vDsp =  
				MSoaGraphBuilder.createExternalMSoaVertexMap(
		    		msoaVertexList);
		
		List<Integer> sourcesIndex = new ArrayList<Integer>();
		List<Integer> sinksIndex = new ArrayList<Integer>();
		
		/* 2. Set node 0 as source */
		sourcesIndex.add(new Integer(0));
	    
		//-----------------------------------------------------------------------------
		for(EdgeAbstract e : connections){
			VertexAbstract from =  e.getFrom();
			VertexAbstract to =  e.getTo();
			int f = -1;
			int t = -1;
			for (MSoaVertex m : msoaVertexList){
				if(m.getType().equals(from.getType()) && 
						m.getName().equals(from.getName())){
					/*System.out.println("from: ");
					System.out.println(m.getType());
					System.out.println(from.getType());
					System.out.println(m.getIndex());
					System.out.println("__________________");*/
					f = m.getIndex();
				}
			}
			for (MSoaVertex m : msoaVertexList){
				if(m.getType().equals(to.getType()) && 
						m.getName().equals(to.getName())){
					/*System.out.println("to: ");
					System.out.println(m.getType());
					System.out.println(to.getType());
					System.out.println(m.getIndex());
					System.out.println("__________________");*/
					t = m.getIndex();
				}
			}
			MSoaEdge ij = new MSoaEdge(f, t);
			msoaEdgeList.add(ij);
			System.out.println(f + " " + t);
	
		}
	
		//---------------------------------------------------------------------
		Map<Pair, MSoaEdge> eDsp =  MSoaGraphBuilder.createExternalMSoaEdgeMap(
	    		msoaEdgeList);
		/* 4. Compute Paths */
		ArrayList<MSoaPath> paths = mbuilder.computePaths(vDsp, eDsp, sourcesIndex);
	
		/* 5. Identify sinks */
		for (MSoaPath path : paths) {
			sinksIndex.add(path.getSink());
		}
		
		MSoaGraphBuilder m = mbuilder.createExternalTopology(
				vDsp,
				eDsp, 
				sourcesIndex,
				sinksIndex,
				paths);
	    m.setCandidate(r);
	    m.printGraph();
	    OptimalSolution op = null;
	    double R = -1;
	    double logA = 1;
	    double amax = 1;
	    double amin = 1;
	    double tmax = -1;
	    double tmin = -1;
	    
	    try {
	    	//idea: in questa versione mando WEIGHT_AVAILABILITY e WEIGHT_RESP_TIME
	    	//via url, e distinguo il caso:
	    	// solo WEIGHT_AVAILABILITY = 1
	    	// solo WEIGHT_AVAILABILITY = 0
	    	if(Double.parseDouble(GlobalConst.WEIGHT_AVAILABILITY)==0.0 && 
	    			Double.parseDouble(GlobalConst.WEIGHT_RESP_TIME) ==1.0){
	    	// misto 
	    		op = App.obtainTMinV3(r, m, params);
	    		R = op.getOptR();
	    		logA = op.getOptLogA();
	    	}
	    	else if(
	    		Double.parseDouble(GlobalConst.WEIGHT_AVAILABILITY)==1.0 && 
		    			Double.parseDouble(GlobalConst.WEIGHT_RESP_TIME) ==0.0){
	    		op = App.obtainAMaxV3(r, m, params);
	    		R = op.getOptR();
	    		logA = op.getOptLogA();
	    	}
	    	else{
		    	// qui calcolo Tmax
	    		tmax = App.obtainTMaxV3(r, m, params).getOptR();
		    	// qui calcolo Amax
	    		amax = App.obtainAMaxV3(r, m, params).getOptLogA();
		    	// qui calcolo Tmin
	    		tmin = App.obtainTMinV3(r, m, params).getOptR();
		    	// qui calcolo Amin
	    		amin = App.obtainAMinV3(r, m, params).getOptLogA();
				// calcolo tutto
		    	//
		    	if(amax>amin){ //posso prevedere una soglia in cui la disponibilità è significativa
			    	op = App.optimalMSOAOrchestrationResolverV3(
							Double.parseDouble(GlobalConst.WEIGHT_AVAILABILITY),
							Double.parseDouble(GlobalConst.WEIGHT_RESP_TIME),
							r,
							m,
							params,
							amax,
							amin,
							tmax,
							tmin
							);
			    	R = op.getOptR();
		    		logA = op.getOptLogA();
		    	}
		    	else{
		    		op =  App.obtainTMinV3(r, m, params);
		    		R = op.getOptR();
		    		logA = op.getOptLogA();
		    	}
	    	}
			System.out.println(op.toString());
			System.out.println(op.toUidString(r));
			System.out.println(op.getSolution(r));
			System.out.println(op.getSolutionVertex(r));
			System.out.println(op.getSolutionEdge(r));
		} catch (ODDException i) {
			// TODO Auto-generated catch block
			i.printStackTrace();
		}
	
	
		// A QUESTO PUNTO VA INSERITA LA JOIN TRA LA SOLUZIONE 
	    // E LE INFO CONTENUTE NEL GRAFO ASTRATTO

		List<ServiceVertex> servicesList =	
				new LinkedList<ServiceVertex>(
						op.getSolutionVertex(r).values());
	
		LinkedList<ServiceEdge> connectionsList = 
				new LinkedList<ServiceEdge>(
						op.getSolutionEdge(r).values());
		DTOResponseConcreteMap dtoResponseConcreteMap  =
					new DTOResponseConcreteMap(); 
		dtoResponseConcreteMap.setServices(new LinkedList<VertexConcrete>());
		dtoResponseConcreteMap.setJoin(new LinkedList<JoinVertexResource>());
		
		for(ServiceVertex s: servicesList){
			VertexConcrete v = new VertexConcrete();
			v.setIp(s.getIp());
			v.setType(s.getType());
			v.setNode(s.getNode());
			v.setUid(s.getId());
			v.setSpeedup(s.getSpeedup());
			v.setAvailability(s.getAvailability());
			dtoResponseConcreteMap.addVertex(v);
		}
		
		for(MSoaVertex l : msoaVertexList){
			System.out.println(l.toString());
			ServiceVertex sv = null; 
			for(ServiceVertex s: servicesList){
				if(s.getIndex() == op.getPlacement(l.getIndex())){
					sv = s;
					break;
				}
			}
			JoinVertexResource j = new JoinVertexResource();
			
			j.setIp(sv.getIp());
			j.setType(sv.getType());
			j.setNode(sv.getNode());
			j.setUid(sv.getId());
			j.setSpeedup(sv.getSpeedup());
			j.setAvailability(sv.getAvailability());
			VertexAbstract va = null;
			for(VertexAbstract n: services){	
				if(n.getName().equals(l.getName())){
					va = n;
					break;
				}
			}
			
			j.setName(va.getName());
			j.setMethod(va.getMethod());
			//j.setUrl(va.getUrl);
			j.setOrder(va.getOrder());
			j.setTo(va.getTo());
			j.setMicroservice(va.getMicroservice());
			j.setIsbody(va.isIsbody());
			j.setBody(va.getBody());
			j.setIsparameters(va.isIsparameters());
			j.setParameters(va.getParameters());
			j.setIsurlconcat(va.isIsurlconcat());
			j.setUrlconcat(va.getUrlconcat());
			j.setEnd(va.isEnd());
			j.setIsbound(va.isIsbound());
			
			
			j.setToip(va.getToip());
			j.setTomicroservices(va.getTomicroservices());
			j.setIstourlconcat(va.getIstourlconcat());
			j.setTourlconcat(va.getTourlconcat());
			j.setIstoparameters(va.getIstoparameters());
			j.setToparameters(va.getToparameters());
			
			
			
			dtoResponseConcreteMap.addJoin(j); 
		}
			if(amax !=1 ){
				dtoResponseConcreteMap.setLogamax(amax);
			}
			if(amin !=1 )
				dtoResponseConcreteMap.setLogamin(amin);
			if(tmax !=-1 )
				dtoResponseConcreteMap.setTmax((int)tmax);
			if(tmin !=-1 )
				dtoResponseConcreteMap.setTmin((int)tmin);
			if(logA !=1 )
				dtoResponseConcreteMap.setLoga(logA);
			if(R != -1 )
				dtoResponseConcreteMap.setR((int) R);
			
			
		ResponseEntity<DTOResponseConcreteMap> responseEntity = 
						new ResponseEntity<DTOResponseConcreteMap>(dtoResponseConcreteMap,HttpStatus.OK);
		return responseEntity;
		
	}

}
