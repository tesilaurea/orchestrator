package org.uniroma2.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="orchestrator")

public class Gqm32Properties {
	
	private String cplexWeightRespTime;
	private String cplexWeightAvailability;
	
	private String appName;
	
	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}
	
	public String getCplexWeightRespTime() {
		return cplexWeightRespTime;
	}

	public void setCplexWeightRespTime(String cplexWeightRespTime) {
		this.cplexWeightRespTime = cplexWeightRespTime;
	}

	public String getCplexWeightAvailability() {
		return cplexWeightAvailability;
	}

	public void setCplexWeightAvailability(String cplexWeightAvailability) {
		this.cplexWeightAvailability = cplexWeightAvailability;
	}	
	
	
}
