package org.uniroma2.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GlobalConst {

	// Define the logger object for this class
	private static final Logger log = LoggerFactory.getLogger(GlobalConst.class);
	 
	
	@Autowired
	Gqm32Properties props;
	

	public static String WEIGHT_AVAILABILITY;
	
	public static String WEIGHT_RESP_TIME;
	
	public static String APPLICATION_NAME;
	
	
	 public GlobalConst (){
		 
	 }
	
	@Value("${orchestrator.cplex-weight-availability}")
	public void setWEIGHT_AVAILABILITY(String wEIGHT_AVAILABILITY) {
		WEIGHT_AVAILABILITY = wEIGHT_AVAILABILITY;
	}
	@Value("${orchestrator.cplex-weight-resp-time}")
	public void setWEIGHT_RESP_TIME(String wEIGHT_RESP_TIME) {
		WEIGHT_RESP_TIME = wEIGHT_RESP_TIME;
	}
	@Value("${spring.application.name}")
	public void setAPPLICATION_NAME(String applicationName) {
		APPLICATION_NAME = applicationName;
	}

	public void someMethod(){
		log.warn(GlobalConst.APPLICATION_NAME);
		log.warn(GlobalConst.WEIGHT_AVAILABILITY);
		log.warn(GlobalConst.WEIGHT_RESP_TIME);
	}

}
