package org.uniroma2.repositories;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import org.uniroma2.model.EdgeConcrete;
import org.uniroma2.model.RedisMicroservice;

@Repository
public class MicroserviceRepositoryImplementation implements MicroserviceRepository{

	private static final String KEY = "Microservice";

	private RedisTemplate<String, Object> redisTemplate;
	private HashOperations<String, String, RedisMicroservice> hashOperations;
	
	@Autowired
	public MicroserviceRepositoryImplementation(RedisTemplate<String, Object> redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	@PostConstruct
	private void init() {
		hashOperations = redisTemplate.opsForHash();
	}

	@Override
	public void save(RedisMicroservice m) {
		hashOperations.put(KEY, m.getUid()+"|"+m.getType()+"|"+m.getName() , m);
	}

	@Override
	public RedisMicroservice find(String id) {
		return hashOperations.get(KEY, id);
	}

	@Override
	public Map<String, RedisMicroservice> findAll() {
		return hashOperations.entries(KEY);
	}

	@Override
	public void update(RedisMicroservice m) {
		hashOperations.put(KEY, m.getUid()+"|"+m.getType()+"|"+m.getName(), m);
	}

	@Override
	public void delete(String id) {
		hashOperations.delete(KEY, id);
	}
}
