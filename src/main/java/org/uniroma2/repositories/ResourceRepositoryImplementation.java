package org.uniroma2.repositories;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import org.uniroma2.model.VertexConcrete;

import com.fasterxml.jackson.databind.ser.std.StringSerializer;

@Repository
public class ResourceRepositoryImplementation implements ResourceRepository{

	private static final String KEY = "Resource";

	private RedisTemplate<String, Object> redisTemplate;
	private HashOperations<String, String, VertexConcrete> hashOperations;
	
			
	@Autowired
	public ResourceRepositoryImplementation(RedisTemplate<String, Object> redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	@PostConstruct
	private void init() {
		hashOperations = redisTemplate.opsForHash();
	}

	@Override
	public void save(VertexConcrete node) {
		hashOperations.put(KEY, node.getUid(), node);
	}

	@Override
	public VertexConcrete find(String uid) {
		return hashOperations.get(KEY, uid);
	}

	@Override
	public Map<String, VertexConcrete> findAll() {
		return hashOperations.entries(KEY);
	}

	@Override
	public void update(VertexConcrete node) {
		hashOperations.put(KEY, node.getUid(), node);
	}

	@Override
	public void delete(String uid) {
		hashOperations.delete(KEY, uid);
	}

}
