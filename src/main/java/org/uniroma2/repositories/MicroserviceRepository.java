package org.uniroma2.repositories;

import java.util.Map;

import org.uniroma2.model.RedisMicroservice;


public interface MicroserviceRepository {

	void save(RedisMicroservice m);
	Map<String, RedisMicroservice> findAll();
	void update(RedisMicroservice m);
	void delete(String id);
	RedisMicroservice find(String id);
}
