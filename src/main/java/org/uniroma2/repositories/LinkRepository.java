package org.uniroma2.repositories;

import java.util.Map;

import org.uniroma2.model.EdgeConcrete;

public interface LinkRepository {

	void save(EdgeConcrete link);
	Map<String, EdgeConcrete> findAll();
	void update(EdgeConcrete link);
	void delete(String uid);
	EdgeConcrete find(String uid);
}
