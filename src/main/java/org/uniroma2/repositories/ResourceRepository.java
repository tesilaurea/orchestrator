package org.uniroma2.repositories;

import java.util.Map;

import org.uniroma2.model.VertexConcrete;

public interface ResourceRepository {
	void save(VertexConcrete node);
	Map<String, VertexConcrete> findAll();
	void update(VertexConcrete node);
	void delete(String uid);
	VertexConcrete find(String uid);
}

	
