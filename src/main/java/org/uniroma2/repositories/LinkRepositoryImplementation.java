package org.uniroma2.repositories;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import org.uniroma2.model.EdgeConcrete;

@Repository
public class LinkRepositoryImplementation implements LinkRepository{

	private static final String KEY = "Link";

	private RedisTemplate<String, Object> redisTemplate;
	private HashOperations<String, String, EdgeConcrete> hashOperations;
	
			
	@Autowired
	public LinkRepositoryImplementation(RedisTemplate<String, Object> redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	@PostConstruct
	private void init() {
		hashOperations = redisTemplate.opsForHash();
	}

	@Override
	public void save(EdgeConcrete link) {
		hashOperations.put(KEY, link.getUid() , link);
	}

	@Override
	public EdgeConcrete find(String uid) {
		return hashOperations.get(KEY, uid);
	}

	@Override
	public Map<String, EdgeConcrete> findAll() {
		return hashOperations.entries(KEY);
	}

	@Override
	public void update(EdgeConcrete link) {
		hashOperations.put(KEY, link.getUid(), link);
	}

	@Override
	public void delete(String uid) {
		hashOperations.delete(KEY, uid);
	}

}
