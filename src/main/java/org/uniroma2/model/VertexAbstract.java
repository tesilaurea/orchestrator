package org.uniroma2.model;

import java.util.LinkedList;

import com.fasterxml.jackson.annotation.JsonInclude;

public class VertexAbstract {

	private static final long serialVersionUID = 1L;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String name;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String method;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String url;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String order;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private LinkedList<String> to;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private LinkedList<String> toip;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private LinkedList<String> tomicroservices;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private LinkedList<String> istourlconcat;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private LinkedList<String> tourlconcat;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private LinkedList<String> istoparameters;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private LinkedList<String> toparameters;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String microservice;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private boolean isbody;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String body;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private boolean isparameters;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private LinkedList<String> parameters;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private boolean isurlconcat;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private LinkedList<String> urlconcat;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private boolean end;	
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String type;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private boolean isbound;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String boundto;	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isEnd() {
		return end;
	}
	public void setEnd(boolean end) {
		this.end = end;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public LinkedList<String> getTo() {
		return to;
	}
	public void setTo(LinkedList<String> to) {
		this.to = to;
	}
	public String getMicroservice() {
		return microservice;
	}
	public void setMicroservice(String microservice) {
		this.microservice = microservice;
	}
	public boolean isIsbody() {
		return isbody;
	}
	public void setIsbody(boolean isbody) {
		this.isbody = isbody;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public boolean isIsparameters() {
		return isparameters;
	}
	public void setIsparameters(boolean isparameters) {
		this.isparameters = isparameters;
	}
	public LinkedList<String> getParameters() {
		return parameters;
	}
	public void setParameters(LinkedList<String> parameters) {
		this.parameters = parameters;
	}
	public boolean isIsurlconcat() {
		return isurlconcat;
	}
	public void setIsurlconcat(boolean isurlconcat) {
		this.isurlconcat = isurlconcat;
	}
	public LinkedList<String> getUrlconcat() {
		return urlconcat;
	}
	public void setUrlconcat(LinkedList<String> urlconcat) {
		this.urlconcat = urlconcat;
	}
	public boolean isIsbound() {
		return isbound;
	}
	public void setIsbound(boolean isbound) {
		this.isbound = isbound;
	}
	public String getBoundto() {
		return boundto;
	}
	public void setBoundto(String boundto) {
		this.boundto = boundto;
	}
	public LinkedList<String> getToip() {
		return toip;
	}
	public void setToip(LinkedList<String> toip) {
		this.toip = toip;
	}
	public LinkedList<String> getTomicroservices() {
		return tomicroservices;
	}
	public void setTomicroservices(LinkedList<String> tomicroservices) {
		this.tomicroservices = tomicroservices;
	}
	public LinkedList<String> getIstourlconcat() {
		return istourlconcat;
	}
	public void setIstourlconcat(LinkedList<String> istourlconcat) {
		this.istourlconcat = istourlconcat;
	}
	public LinkedList<String> getTourlconcat() {
		return tourlconcat;
	}
	public void setTourlconcat(LinkedList<String> tourlconcat) {
		this.tourlconcat = tourlconcat;
	}
	public LinkedList<String> getIstoparameters() {
		return istoparameters;
	}
	public void setIstoparameters(LinkedList<String> istoparameters) {
		this.istoparameters = istoparameters;
	}
	public LinkedList<String> getToparameters() {
		return toparameters;
	}
	public void setToparameters(LinkedList<String> toparameters) {
		this.toparameters = toparameters;
	}
	
	
	
	
}
