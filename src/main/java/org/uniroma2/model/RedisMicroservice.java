package org.uniroma2.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RedisMicroservice implements Serializable{

	private static final long serialVersionUID = 1L;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String uid;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String type;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String deployment;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String name;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private int tu;
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDeployment() {
		return deployment;
	}
	public void setDeployment(String deployment) {
		this.deployment = deployment;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getTu() {
		return tu;
	}
	public void setTu(int tu) {
		this.tu = tu;
	}
	@Override
	public String toString() {
		return "RedisMicroservice [uid=" + uid + ", type=" + type + ", deployment=" + deployment + ", name=" + name
				+ ", tu=" + tu + "]";
	}
	public RedisMicroservice(String uid, String type, String deployment, String name, int tu) {
		super();
		this.uid = uid;
		this.type = type;
		this.deployment = deployment;
		this.name = name;
		this.tu = tu;
	}
	public RedisMicroservice() {
		super();
	}
	
	
	
}
