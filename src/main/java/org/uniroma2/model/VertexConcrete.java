package org.uniroma2.model;

import java.io.Serializable;
import java.util.LinkedList;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class VertexConcrete implements Serializable {
	//Sto facendo l'ipotesi forte che uid sia un numero progressivo numerico 
	// (magari come un token che viene assegnato e incrementato in maniera atomico)
	private static final long serialVersionUID = 1L;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String uid;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String name;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private boolean end;		  
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String type;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String node;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String ip;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	protected double speedup;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	protected double availability;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	protected LinkedList<Microservice> microservices = new LinkedList<Microservice>(); 
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isEnd() {
		return end;
	}
	public void setEnd(boolean end) {
		this.end = end;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	
	public String getNode() {
		return node;
	}
	public void setNode(String node) {
		this.node = node;
	}
	
	public double getSpeedup() {
		return speedup;
	}
	public void setSpeedup(double speedup) {
		this.speedup = speedup;
	}
	public double getAvailability() {
		return availability;
	}
	public void setAvailability(double availability) {
		this.availability = availability;
	}
	public VertexConcrete() {
		super();
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public VertexConcrete(String uid, String name, boolean end, String type, String node, String ip, double speedup,
			double availability) {
		super();
		this.uid = uid;
		this.name = name;
		this.end = end;
		this.type = type;
		this.node = node;
		this.ip = ip;
		this.speedup = speedup;
		this.availability = availability;
	}
	@Override
	public String toString() {
		return "VertexConcrete [uid=" + uid + ", name=" + name + ", end=" + end + ", type=" + type + ", node=" + node
				+ ", ip=" + ip + ", speedup=" + speedup + ", availability=" + availability + ", microservices="
				+ microservices + "]";
	}
	public LinkedList<Microservice> getMicroservices() {
		return microservices;
	}
	public void setMicroservices(Microservice microservice) {
		this.microservices.add(microservice);
	}
	
	public LinkedList<org.uniroma2.tesi.codd.model.Microservice> getConvMicroservices() {
		LinkedList<org.uniroma2.tesi.codd.model.Microservice> list = 
				new LinkedList<org.uniroma2.tesi.codd.model.Microservice>();
		for (Microservice m : microservices ){
			org.uniroma2.tesi.codd.model.Microservice orgM = new org.uniroma2.tesi.codd.model.Microservice(
					m.getName(), m.getTu());
			list.add(orgM);
		}
		return list;
	}
	
	
}


