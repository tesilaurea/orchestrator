package org.uniroma2.model;

import com.fasterxml.jackson.annotation.JsonInclude;

public class EdgeAbstract {

	private static final long serialVersionUID = 1L;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private VertexAbstract from;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private VertexAbstract to;
	public VertexAbstract getFrom() {
		return from;
	}
	public void setFrom(VertexAbstract from) {
		this.from = from;
	}
	public VertexAbstract getTo() {
		return to;
	}
	public void setTo(VertexAbstract to) {
		this.to = to;
	}
	
}
