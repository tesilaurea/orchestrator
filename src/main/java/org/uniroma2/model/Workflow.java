package org.uniroma2.model;

import java.util.LinkedList;

import com.fasterxml.jackson.annotation.JsonInclude;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class Workflow {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String uid;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private int numelem;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String startat;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private LinkedList<VertexConcrete> services;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private LinkedList<EdgeConcrete> connections;
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public int getNumelem() {
		return numelem;
	}
	public void setNumelem(int numelem) {
		this.numelem = numelem;
	}
	public String getStartat() {
		return startat;
	}
	public void setStartat(String startat) {
		this.startat = startat;
	}
	public LinkedList<VertexConcrete> getServices() {
		return services;
	}
	public void setServices(LinkedList<VertexConcrete> services) {
		this.services = services;
	}
	public LinkedList<EdgeConcrete> getConnections() {
		return connections;
	}
	public void setConnections(LinkedList<EdgeConcrete> connections) {
		this.connections = connections;
	}
	
	
}


