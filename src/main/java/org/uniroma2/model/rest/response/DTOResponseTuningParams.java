package org.uniroma2.model.rest.response;

import org.uniroma2.model.rest.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class DTOResponseTuningParams extends DTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonInclude(Include.NON_NULL)
	public String wa;
	@JsonInclude(Include.NON_NULL)
	public String wr;
	public String getWa() {
		return wa;
	}
	public void setWa(String wa) {
		this.wa = wa;
	}
	public String getWr() {
		return wr;
	}
	public void setWr(String wr) {
		this.wr = wr;
	}
	public DTOResponseTuningParams() {
		super();
	}
	
}