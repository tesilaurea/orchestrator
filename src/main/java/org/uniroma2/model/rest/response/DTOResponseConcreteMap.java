package org.uniroma2.model.rest.response;

import java.util.LinkedList;

import org.uniroma2.model.EdgeConcrete;
import org.uniroma2.model.JoinVertexResource;
import org.uniroma2.model.VertexConcrete;
import org.uniroma2.model.rest.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class DTOResponseConcreteMap extends DTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonInclude(Include.NON_NULL)
	private LinkedList<VertexConcrete> services;
	
	private LinkedList<JoinVertexResource> join;
	
	@JsonInclude(Include.NON_DEFAULT)
	private int tmax;
	@JsonInclude(Include.NON_DEFAULT)
	private int tmin;
	@JsonInclude(Include.NON_DEFAULT)
	private double logamax;
	@JsonInclude(Include.NON_DEFAULT)
	private double logamin;
	@JsonInclude(Include.NON_NULL)
	private int r;
	@JsonInclude(Include.NON_NULL)
	private double loga;
	
	@JsonInclude(Include.NON_NULL)
	private LinkedList<EdgeConcrete> connections;
	public LinkedList<VertexConcrete> getServices() {
		return services;
	}
	public void setServices(LinkedList<VertexConcrete> services) {
		this.services = services;
	}
	
	
	public LinkedList<JoinVertexResource> getJoin() {
		return join;
	}
	public void setJoin(LinkedList<JoinVertexResource> join) {
		this.join = join;
	}
	public LinkedList<EdgeConcrete> getConnections() {
		return connections;
	}
	public void setConnections(LinkedList<EdgeConcrete> connections) {
		this.connections = connections;
	}
	public void addVertex(VertexConcrete v) {
		this.services.add(v);
	}
	
	public void addEdge(EdgeConcrete e) {
		this.connections.add(e);
	}
	
	public void addJoin(JoinVertexResource j) {
		this.join.add(j);
	}
	public int getTmax() {
		return tmax;
	}
	public void setTmax(int tmax) {
		this.tmax = tmax;
	}
	public int getTmin() {
		return tmin;
	}
	public void setTmin(int tmin) {
		this.tmin = tmin;
	}
	public double getLogamax() {
		return logamax;
	}
	public void setLogamax(double logamax) {
		this.logamax = logamax;
	}
	public double getLogamin() {
		return logamin;
	}
	public void setLogamin(double logamin) {
		this.logamin = logamin;
	}
	public int getR() {
		return r;
	}
	public void setR(int r) {
		this.r = r;
	}
	public double getLoga() {
		return loga;
	}
	public void setLoga(double loga) {
		this.loga = loga;
	}
	
	
	
	
}
