package org.uniroma2.model.rest;

import java.util.LinkedList;

import org.uniroma2.model.EdgeAbstract;
import org.uniroma2.model.EdgeConcrete;
import org.uniroma2.model.VertexAbstract;
import org.uniroma2.model.VertexConcrete;

import com.fasterxml.jackson.annotation.JsonInclude;

public class DTOWorkflow extends DTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String uid;
	private int numelem;
	private String startat;
	private LinkedList<VertexAbstract> services;
	private LinkedList<EdgeAbstract> connections;
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public int getNumelem() {
		return numelem;
	}
	public void setNumelem(int numelem) {
		this.numelem = numelem;
	}
	public String getStartat() {
		return startat;
	}
	public void setStartat(String startat) {
		this.startat = startat;
	}
	public LinkedList<VertexAbstract> getServices() {
		return services;
	}
	public void setServices(LinkedList<VertexAbstract> services) {
		this.services = services;
	}
	public LinkedList<EdgeAbstract> getConnections() {
		return connections;
	}
	public void setConnections(LinkedList<EdgeAbstract> connections) {
		this.connections = connections;
	}
	
	
	
}




