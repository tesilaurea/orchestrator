package org.uniroma2.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EdgeConcrete implements Serializable {

	private static final long serialVersionUID = 1L;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String uid;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private VertexConcrete from;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private VertexConcrete to;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Double delay;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Double availability;
	
	public VertexConcrete getFrom() {
		return from;
	}
	public void setFrom(VertexConcrete from) {
		this.from = from;
	}
	public VertexConcrete getTo() {
		return to;
	}
	public void setTo(VertexConcrete to) {
		this.to = to;
	}
	public Double getDelay() {
		return delay;
	}
	public void setDelay(Double delay) {
		this.delay = delay;
	}
	public Double getAvailability() {
		return availability;
	}
	public void setAvailability(Double availability) {
		this.availability = availability;
	}
	public EdgeConcrete(String uid, VertexConcrete from, VertexConcrete to, Double delay, Double availability) {
		super();
		this.uid = uid;
		this.from = from;
		this.to = to;
		this.delay = delay;
		this.availability = availability;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	@Override
	public String toString() {
		return "Link [uid=" + uid + ", from=" + from + ", to=" + to + ", delay=" + delay + ", availability="
				+ availability + "]";
	}
	public EdgeConcrete() {
		super();
	}
	
	
	
	
}
